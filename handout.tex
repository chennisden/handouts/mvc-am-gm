\documentclass{marticl}

\usepackage[margin=2cm]{geometry}

\usepackage{newpxtext,newpxmath} % fonts

\usepackage{amsmath}
\usepackage{asymptote}
	\begin{asydef}
		usepackage("newpxtext, newpxmath");
	\end{asydef}

\usepackage[noscramble]{scrambledenvs}
	\newscrambledenv{solu}
		\solulabel{solution}

\title{Inequalities}
\author{Dennis Chen}

\begin{document}

\maketitle

\toc

\section{AM-GM}

AM-GM is probably the most basic inequality, and it forms the basis for more than half of the techniques we present. It helps solve problems like, ``find the maximum value $xyz$ attains over all triples of positive reals $x$, $y$, $z$ such that $4xy+2yz+zx=48$.''

\begin{theo}[AM-GM]
	Given positive reals $x_1, x_2, \ldots, x_n$,
	\[\frac{x_1+x_2+\cdots+x_n}{n}\geq\sqrt[n]{x_1x_2\ldots x_n}.\]
\end{theo}

For clarity, let's look at what AM-GM says when there are two variables and convince ourselves it's true.

\begin{exam}[2 Variable AM-GM]
	Given positive reals $x$, $y$, prove that $\frac{x+y}{2}\geq \sqrt{xy}$.
\end{exam}

\begin{sol}
	Note this is equivalent to $x+y\geq 2\sqrt{xy}$, or $(\sqrt{x}-\sqrt{y})^2\geq 0$.
\end{sol}

There are a couple of ways to prove AM-GM in general. The most obvious for a vector calculus student is the method of Lagrange multipliers.

\begin{pro}
	Let us fix the arithmetic mean as $\bar{x}$ and find the largest possible product. Note we essentially want to maximize $f(x)=x_1x_2\ldots x_n$ given a constraint of $g(x)=x_1+x_2+\cdots+x_n-n\bar{x}=0$.

	By LM, this occurs when
	
	\begin{align*}
		\nabla\vec{f} &= \lambda\nabla\vec{g} \\
		\langle \frac{x_1\ldots x_n}{x_1}, \frac{x_1\ldots x_n}{x_2}, \ldots, \frac{x_1\ldots x_n}{x_n} \rangle &= \lambda \langle 1, 1, \ldots, 1 \rangle.
	\end{align*}
	
	This implies $x_i=\frac{x_1\ldots x_n}{\lambda}$ for all $i$, meaning that all $x_i$ are equal when we attain our maximum. Plugging in $x_i=\bar{x}$ reveals that the maximum value of $x_1x_2\ldots x_n$ is $\bar{x}^n$, or that $\bar{x}\geq \sqrt[n]{x_1x_2\ldots x_n}$ as desired.
\end{pro}

But there's a way to prove AM-GM with just Algebra 1 concepts! (It's not that simple though, a lot of logic and intuition is required to come up with it!)

\begin{pro}
	Again, we fix the arithmetic mean as $\bar{x}$. This time, we prove the maximum product of all terms is NOT achieved when there are two non-equal terms (i.e. we can find a greater product with the same sum).

	Say $x\neq y$ are two of our values. Then replacing $x$, $y$ with two copies of $\frac{x+y}{2}$ increases the GM but does not change the AM.

	Since we can always increase the GM except when all terms are equal, we achieve our maximum when all terms are equal.
\end{pro}

This argument, however, does have a subtle element: we must first prove a maximum exists. Fortunately, we can establish an upper bound by noting $0<x_i \leq n\bar{x}$ which implies $x_1x_2\ldots x_n\leq (n\bar{x})^n$.

However, establishing an upper bound is not enough. For instance, the set of all positive numbers strictly less than $1$ has an upper bound of $1$, yet it has no maximum. However, since $x_1x_2\ldots x_n$ is a continuous function\footnote{We can arbitrarily allow terms to be $0$ for the sake of this argument; it does not change the proof at all.} it must have a maximum. So we see that even with this proof, we cannot really escape calculus.

\subsection{All the means: QM-AM-GM-HM}

It might be good to skip ahead and read the Cauchy section first.

There are more types of means --- the quadratic mean and harmonic mean are the two we will be focusing on. Let's first define them.

\begin{defi}[QM]
	The quadratic mean of positive reals $x_1, x_2, \ldots, x_n$ is $\sqrt{\frac{x_1^2+x_2^2+\cdots+x_n^2}{n}}$.
\end{defi}

\begin{defi}[HM]
	The harmonic mean of positive reals $x_1, x_2, \ldots, x_n$ is $\frac{n}{\frac{1}{x_1}+\cdots+\frac{1}{x_n}}$.
\end{defi}

As a reminder, if a car travels stretches of one mile at speeds $x_1, x_2, \ldots, x_n$, then its average speed along the whole journey is the harmonic mean.

\begin{theo}[QM-AM-GM-HM]
	Given positive reals $x_1, x_2, \ldots, x_n$,
	\[\sqrt{\frac{x_1^2+x_2^2+\cdots+x_n^2}{n}}\geq \frac{x_1+x_2+\cdots+x_n}{n}\geq \sqrt[n]{x_1x_2\ldots x_n}\geq \frac{n}{\frac{1}{x_1}+\cdots+\frac{1}{x_n}}.\]

	In short, $QM\geq AM\geq GM\geq HM$.
\end{theo}

\begin{pro}
	We essentially can split this problem up into proving $QM\geq AM$ and $GM\geq HM$. (Recall that we already proved $AM\geq GM$.)

	To prove $QM\geq AM$, note that we want to show $\frac{x_1^2+\cdots +x_n^2}{n}\geq \frac{(x_1+\cdots +x_n)^2}{n^2}$, or $n(x_1^2+\cdots +x_n^2)\geq (x_1+\cdots+x_n)^2$.

	But if we consider the vectors $\vec{u} = \langle 1, 1, \ldots, 1\rangle$ and $\vec{v} = \langle x_1, x_2, \ldots, x_n \rangle$, then this inequality is equivalent to
	\[|\vec{u}|^2 |\vec{v}|^2\geq (\vec{u}\cdot \vec{v})^2,\]
	which follows fairly easily as $\vec{u}\cdot \vec{v}=|\vec{u}||\vec{v}|\cos\theta$ where $\theta$ is the angle between the vectors and $-1\leq \cos \theta \leq 1$.

	To prove $GM\geq HM$, we can just use AM-GM. We want to show 
	\[\sqrt[n]{x_1x_2\ldots x_n}(\frac{1}{x_1}+\cdots+\frac{1}{x_n})\geq n,\]
	which is true as the product of every addend is $1$.
\end{pro}

\subsection{Examples}

\begin{exam}
	Find the minimum value $\frac xy + \frac yz + \frac zx$ can take over positive reals $x$, $y$, $z$, and describe when equality observes.
\end{exam}

\begin{sol}
	Note that by AM-GM,
	\[\frac xy + \frac yz + \frac zx \geq 3 \sqrt[3]{\frac xy \cdot \frac yz \cdot \frac zx} = 3,\]
	with equality when $\frac xy = \frac yz = \frac zx$.
	
	This equality condition is satisfied when $x=y=z$.
\end{sol}

\begin{exam}
	Prove that $2x^3+y^6 \geq 3x^2y^2$.
\end{exam}

\begin{sol}
	Instead of mindlessly trying to apply AM-GM on $2x^3$ and $y^6$, notice that the exponents for $x$ and $y$ are equivalent. This motivates applying AM-Gm on $x^3$, $x^3$, and $y^6$ instead, which gives us
	\[x^3 + x^3 + y^6 \geq 3\sqrt[3]{x^3\cdot x^3\cdot y^6} = 3x^2y^2.\]
	Equality occurs when $x^3=x^3=y^6$, or $x=y^2$.
\end{sol}

\begin{exam}
	Prove $a^3+b^3+c^3\geq a^2b+b^2c+c^2a$ for positive reals $a$, $b$, and $c$.
\end{exam}

\begin{sol}
	This time around, we do not directly use AM-GM on all the terms. Instead, we note
	\begin{align*}
		\frac{a^3+a^3+b^3}{3} &\geq a^2b \\
		\frac{b^3+b^3+c^3}{3} &\geq b^2c \\
		\frac{c^3+c^3+a^3}{3} &\geq c^2a
	\end{align*}
	Adding these inequalities gives us our desired result.
\end{sol}

\subsection{Exercises}

\begin{prob}
	Prove that $a^2b+b^2c+c^2a\geq 3abc$ for positive reals $a$, $b$, $c$.
	\begin{solus}
		\begin{solu}
			Note that $a^2b+b^2c+c^2a \geq 3\sqrt[3]{a^2b\cdot b^2c\cdot c^2a} = 3abc$.
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}
	Find the minimum value $3+3^x+4+4^x+2+2^{-x}+6+6^{-x}$ can take.
	\begin{solus}
		\begin{solu}
			Apply AM-GM on only the non-constant terms. Note that $3^x+4^x+2^{-x}+6^{-x}\geq 4\sqrt[4]{3^x\cdot 4^x\cdot 2^{-x}\cdot 6^{-x}}=4$, so the whole sum is at least $(3+4+2+6)+4=19$.
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}
	A student runs $100$ meters on a moving walkway and back at a constant speed of $10\frac ms$ with respect to the walkway, and then he turns around and runs the same $100$ meters back. If the walkway is also moving at a constant rate, prove that he must have taken $20$ seconds or more.
	\begin{solus}
		\begin{solu}
			Say the walkway was moving at a speed of $s$. Then his speeds were $10-s$ and $10+s$, and the time he took was $\frac{100}{10-s}+\frac{100}{10+s}$, and his speed was $\frac{200}{\frac{100}{10-s}+\frac{100}{10+s}} = \frac{2}{\frac{1}{10-s}+\frac{1}{10+s}}$. But by AM-HM on $10-s$ and $10+s$,
			\[\frac{(10-s)+\frac(10+s)}{2} \geq \frac{2}{\frac{1}{10-s}+\frac{1}{10+s}},\]
			implying that the largest his average speed could be is $10 \frac ms$. Therefore, he must have taken at least $20$ seconds.
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}[AIME 1983/9]
	Find the minimum value of $\frac{9x^2\sin^2 x + 4}{x\sin x}$ for $0 < x < \pi$.
	\begin{solus}
		\begin{solu}
			By AM-GM,
			\[9x\sin x + \frac{4}{x\sin x}\geq 2\sqrt{36} = 12.\]
			Of course, we have to establish that equality is possible, i.e. $9x\sin x = \frac{4}{x\sin x}$, or $x\sin x = \frac{3}{2}$. Since $x\sin x$ is a continuous function and $\frac{\pi}{2}\sin(\frac{\pi}{2})=\frac{\pi}{2}>\frac{3}{2}$, equality can be achieved.
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}
	Prove the area of a triangle with fixed perimeter is maximized when it is equilateral.\footnote{It may be useful to know Heron's formula for the area of a triangle: $\sqrt{s(s-a)(s-b)(s-c)}$.}
\end{prob}


\begin{prob}[Math Advance]
	Find the maximum value of $k$ such that $(x+1)^4\geq kx^3$ for all real $x$.
	\begin{solus}
		\begin{solu}
			Obviously $k$ is non-negative, since $k=0$ just works. We can assume that $x$ is positive, because if $x$ is not, $(x+1)^4\geq 0\geq kx^3$.

			By AM-GM, $\frac{x}{3}+\frac{x}{3}+\frac{x}{3}+1\geq 4\sqrt[4]{\frac{x}{3}\cdot \frac{x}{3}\cdot \frac{x}{3}\cdot 1}$, implying that
			\[(x+1)^4=(\frac{x}{3}\cdot 3+1)^4\geq \frac{4^4}{3^3}x^3.\]
			So the answer is $k\leq \frac{4^4}{3^3}$.
		\end{solu}
	\end{solus}
\end{prob}

\section{Cauchy}

The Cauchy-Schwarz inequality just states that the dot product of two vectors is less than the product of their magnitudes.

\begin{theo}[Cauchy-Schwarz Inequality]
	Given vectors $\vec{u}$ and $\vec{v}$,
	\[\vec{u}\cdot \vec{v} \leq |\vec{u}||\vec{v}|.\]
\end{theo}

This seems obvious and not useful at first glance. However, it is also equivalent to
\[(u_1v_1+\cdots+u_nv_n)^2\leq (u_1^2+\cdots+u_n^2)(v_1^2+\cdots+v_n^n),\]
which is very useful.

\subsection{Examples}

\begin{exam}
	Prove that for positive reals $a$, $b$, $c$, $d$, $e$,
	\[(\frac ab + \frac bc + \frac ca)^2 \leq 3(\frac{a^2}{b^2}+\frac{b^2}{c^2}+\frac{c^2}{a^2}).\]
\end{exam}

\begin{sol}
	This is true by Cauchy on $\langle \frac ab, \frac bc, \frac ca\rangle$ and $\langle 1, 1, 1\rangle$.
\end{sol}

\begin{prob}
	As a followup, prove that $\frac ab + \frac bc + \frac ca \leq \frac{a^2}{b^2}+\frac{b^2}{c^2}+\frac{c^2}{a^2}$.
	\begin{solus}
		\begin{solu}
			We've already shown by Cauchy that
			\[(\frac ab + \frac bc + \frac ca)^2 \leq 3(\frac{a^2}{b^2}+\frac{b^2}{c^2}+\frac{c^2}{a^2}).\]
			Now, by AM-GM, $3\leq \frac ab + \frac bc + \frac ca$, so
			\[3(\frac{a^2}{b^2}+\frac{b^2}{c^2}+\frac{c^2}{a^2})\leq (\frac ab + \frac bc + \frac ca)(\frac{a^2}{b^2}+\frac{b^2}{c^2}+\frac{c^2}{a^2}).\]
			Thus,
			\begin{align*}
				(\frac ab + \frac bc + \frac ca)^2 &\leq (\frac ab + \frac bc + \frac ca)(\frac{a^2}{b^2} + \frac{b^2}{c^2} + \frac{c^2}{a^2}) \\
				\frac ab + \frac bc + \frac ca &\leq \frac{a^2}{b^2} + \frac{b^2}{c^2} + \frac{c^2}{a^2},
			\end{align*}
			as desired.
		\end{solu}
	\end{solus}
\end{prob}

\begin{exam}
	Prove that for positive reals $a$, $b$, $c$,
	\[(a+b+c)(\frac 1a + \frac 1b + \frac 1c) \geq 9.\]
\end{exam}

\begin{sol}
	This is true by Cauchy on $\langle \sqrt a, \sqrt b, \sqrt c\rangle$ and $\langle \sqrt{\frac 1a}, \sqrt{\frac 1b}, \sqrt{\frac 1c} \rangle$.
\end{sol}

Alternatively, it is possible to use AM-HM to prove this, but it is a bit harder to notice.

\begin{sol}
	Note that by AM-HM,
	\[\frac{a+b+c}{3}\geq \frac{3}{\frac 1a + \frac 1b + \frac 1c}.\]
	Rearranging gives
	\[(a+b+c)(\frac 1a + \frac 1b + \frac 1c)\geq 9\]
	as desired.
\end{sol}

\subsection{Exercises}

\begin{prob}
	What is the equality condition for Cauchy? State it in both the vector form and the real number form (i.e. $(u_1v_1+\cdots+u_nv_n)^2\leq(u_1^2+\cdots +u_n^2)(v_1^2+\cdots+v_n^2)$).
\end{prob}

\begin{prob}[Mildorf]
	Show that for all positive reals $a$, $b$, $c$, $d$,
	\[\frac 1a + \frac 1b + \frac 4c + \frac{16}d \geq \frac{64}{a+b+c+d}.\]
\end{prob}

\begin{prob}[Titu's Lemma]
	Prove that for positive reals $u_1,\ldots u_n$ and $v_1,\ldots, v_n$,
	\[\frac{u_1^2}{v_1}+\cdots+\frac{u_n^2}{v_n}\geq \frac{(u_1+\cdots+u_n)^2}{v_1+v_2+\cdots+v_n}.\]
\end{prob}

\begin{prob}[Nesbitt's Inequality]
	Prove that for positive reals $a$, $b$, $c$,
	\[\frac{a}{b+c}+\frac{b}{c+a}+\frac{c}{a+b}\geq \frac 32.\]
	\begin{solus}
		\begin{solu}
			Note that this is equivalent to proving $2(a+b+c)(\frac{1}{b+c}+\frac{1}{c+a} + \frac{1}{a+b})\geq 9$. Let $x=b+c$, $y=c+a$, and $z=a+b$. Then this reduces to $(x+y+z)(\frac 1x + \frac 1y + \frac 1z)\geq 9$, which we have already shown is true by either AM-HM or Cauchy before.
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}
	Prove that for positive real numbers $a$, $b$,
	\[8(a^4+b^4)\geq (a+b)^4.\]
\end{prob}

\begin{prob}[IMO 1995/2]
	Let $a$, $b$, and $c$ be positive reals with $abc=1$. Prove that
	\[\frac{1}{a^3(b+c)}+\frac{1}{b^3(c+a)}+\frac{1}{c^3(a+b)}\geq \frac 32.\]
\end{prob}

\section{A Potpourri of Calculus}

Calculus is still useful, even in math olympiads. Whether it is used directly or not, it can be used as a method to find solutions to inequalities.

\subsection{Convex Functions}

\begin{defi}[Convex Function]
	A function $f$ is convex on an interval $(a,b)$ if for all $x_1$, $x_2$ in that interval,
	\[f(tx_1+(1-t)x_2)\leq tf(tx_1)+(1-t)f(x_2).\]
	for all reals $0\leq t\leq 1$.
\end{defi}

Graphically, this means that if we connect any two points $x_1$ and $x_2$, the points in between on that function lie below the connecting line segment.

\begin{center}
	\begin{asy}
		size(4cm);
		import graph;
		real f(real x) {
			return x*x;
		}
		draw(graph(f, -1, 1));
		draw((-0.5,0.25)--(0.9,0.81));
	\end{asy}
\end{center}

In $f\colon \mathbb{R}\to \mathbb{R}$, all convex functions on an open interval are continuous. This is true as for all $a<x_1< x_2<x_3<b$,
\[\frac{f(x_2)-f(x_1)}{x_2-x_1}\leq \frac{f(x_3)-f(x_1)}{x_3-x_1}\leq \frac{f(x_3)-f(x_2)}{x_3-x_2},\]
showing that both a left and right derivative exists for the point $x_3$.

Note that it must be an \emph{open} interval because $f(x)=-\sqrt{x}$ for $x>0$ and $f(0)=1$ is a convex, non-continuous function.

There is an alternative way to define convex functions with second derivatives. We will show that they are equivalent.

\begin{theo}
	A function is convex on an interval if and only if its second derivative is non-negative along that interval. To state it symbolically, $f$ is convex iff $f'' \geq 0$.
\end{theo}

\begin{pro}
	When we showed that $f$ was continuous, we also showed that the slope of secant lines can be continually bounded as two points get closer together, i.e. the first derivative of $f$ is also continuous. Also, by the definition of concavity, if $w<x<y<z$, the slope of the secant line joining $w$ and $x$ is less than the slope of the secant line joining $x$ and $y$ which is less than the slope of the secant line joining $y$ and $z$. By pushing $x$ close to $w$ and $y$ close to $z$, we can say the tangent line at $x$ has a smaller slope than the tangent line at $z$.
\end{pro}

Concave functions also exist, but for inequalities on the positive reals it is rare that we use them. They are essentially equivalent to convex functions, only negative.

\begin{defi}[Concave Function]
	A function $f$ is concave if $-f$ is convex.
\end{defi}

We can generalize the given condition of a convex function using Jensen's Inequality.

\begin{theo}[Jensen's Inequality]
	Given reals $x_1,\ldots,x_n$, positive weights $\lambda_1,\ldots,\lambda_n$ that sum to $1$, and convex function $f$,
	\[\lambda_1x_1+\cdots+\lambda_nx_n\geq f(\lambda_1x_1+\cdots+\lambda_nx_n).\]
\end{theo}

\begin{pro}
	We proceed by induction. Our base case is $n=2$ by the definition of a convex function.

	Now we want to prove our hypothesis for $n+1$ if it is true for $n$. Let $x=\frac{\lambda_1x_1+\cdots+\lambda_nx_n}{1-\lambda_{n+1}}$. By the definition of a convex function on $x_{n+1}$ and $x$, using $t=\lambda$,
	\[f(\lambda_{n+1}x_{n+1}+(1-\lambda_{n+1})x_{n+1}) = f(\lambda_1x_1+\cdots+\lambda_{n+1}x_{n+1}) \leq \lambda_{n+1}f(x_{n+1})+(1-\lambda_{n+1})f(x).\]
	But by Jensen on the terms inside $x$,
	\[f(x)\leq \frac{\lambda_1f(x_1)+\cdots+\lambda_nf(x_n)}{1-\lambda_{n+1}},\]
	since $\lambda_1+\cdots+\lambda_n=1-\lambda_{n+1}$.

	Substituting will yield the desired conclusion.
\end{pro}

The direction of the inequality sign is flipped for concave functions.

\begin{prob}
	Prove that $f(x)=\ln x$ is a concave function.
	\begin{solus}
		\begin{solu}
			Note that $f''(x)=-\frac{1}{x^2}$ which is always negative, so $f$ is convex.
		\end{solu}
	\end{solus}
\end{prob}

\begin{exam}[Weighted AM-GM]
	Prove that given positive weights $\lambda_1, \lambda_2, \ldots, \lambda_n$ that sum to $1$,
	\[\lambda_1x_1+\cdots+\lambda_nx_n\geq x_1^{\lambda_1}\cdots x_n^{\lambda_n}.\]
\end{exam}

\begin{pro}
	Let $f(x)=\ln x$. Since $f$ is concave, by Jensen's
	\[f(\lambda_1x_1+\cdots+\lambda_nx_n)\geq\lambda_1f(x_1)+\cdots+\lambda_1f(x_n).\]
	This is true for any generic concave function. Substituting in $f(x)=\ln x$ gives
	\[\ln(\lambda_1x_1+\cdots+\lambda_nx_n)\geq \ln(x_1^{\lambda_1}\cdots x_n^{\lambda_n}).\]
	Raising $e$ to the power of both sides of this inequality gives
	\[\lambda_1x_1+\cdots+\lambda_nx_n\geq x_1^{\lambda_1}\cdots x_n^{\lambda_n}.\]
\end{pro}

\begin{exam}[Vietnam 1998]
	Let $x_1,\ldots,x_n$ be positive reals such that
	\[\frac{1}{x_1+1998}+\cdots+\frac{1}{x_n+1998}=\frac{1}{1998}.\]
	Prove that
	\[\frac{\sqrt[n]{x_1x_2\cdots x_n}}{n-1}\geq 1998.\]
\end{exam}

\begin{sol}
	Let $y_i=\frac{1998}{1998+x_i}$. Note that $\sum y_i = 1$ and $x_i = 1998(\frac{1}{y_i}-1)$, and we now want to prove that
	\[\prod (\frac{1}{y_i}-1)\geq (n-1)^n.\]
	Using a similar trick we did for weighted AM-GM, set $f(y)=\ln(\frac{1}{y}-1)$. Taking the natural log of both sides, the inequality then becomes
	\[f(y_1)+\cdots+f(y_n)\geq nf(\frac{1}{n}).\]
	Note $f''(y)=\frac{-2y+1}{y^2(-y+1)^2}$, which is concave when $y\leq-\frac{1}{2}$. We can't quite employ Jensen's yet, but we're close: all we have to do is show no $y_i$ needs to be greater than $\frac{1}{2}$.

	Let's say we have $y_1>\frac 12$ and $y_2 < \frac 12$. Then we'll replace $y_1$ and $y_2$ with $\frac{y_1+y_2}{2}$, and we want to show
	\begin{align*}
		(\frac{1}{y_1}-1)(\frac{1}{y_2}-1) &\geq (\frac{2}{y_1+y_2}-1)^2 \\
		\frac{1}{y_1y_2}-\frac{1}{y_1}-\frac{1}{y_2} &\geq \frac{4}{(y_1+y_2)^2}-\frac{4}{y_1+y_2} \\
		(y_1+y_2)^2-(y_1+y_2)^3 &\geq 4y_1y_2-4y_1y_2(y_1+y_2) \\
		(y_1+y_2)^2(1-y_1-y_2) &\geq 4y_1y_2(1-y_1-y_2) \\
		(y_1+y_2)^2 &\geq 4y_1y_2 \\
		(y_1-y_2)^2 &\geq 0.
	\end{align*}
	Since every step is reversible, we have proved that we can replace two terms with their average and decrease the product. Therefore, if this inequality is true when no $y_i$ exceeds $\frac 12$, it is always true.

	Now, since $f$ is concave,
	\[f(y_1)+\cdots+f(y_n)\geq nf(\frac{1}{n}).\]
	Raising $e$ to the power of both sides gives us our desired conclusion.
\end{sol}

\begin{prob}
	Show that in $\triangle ABC$, $\sin A + \sin B + \sin C \leq \frac{3\sqrt{3}}{2}$.
	\begin{solus}
		\begin{solu}
			Note that the second derivative of $f(x)=\sin x$ is $f''(x)=-\sin x$, which is negative for all $0\leq x\leq \pi$.
			Therefore, by Jensen's,
			\[\sin A + \sin B + \sin C \leq 3\sin(\frac{A+B+C}{3}) = \frac{3\sqrt{3}}{2}.\]
		\end{solu}
	\end{solus}
\end{prob}

\begin{prob}
	Show that in $\triangle ABC$, $\cos A \cos B \cos C \leq \frac 18$.
	\begin{solus}
		\begin{solu}
			This is equivalent to showing that $\ln \cos A + \ln \cos B + \ln cos C \leq \ln \frac 18$.
			Since $\frac{d}{dx} \ln \cos x = -\frac{\sin x}{\cos x} = -\tan x$ and $\frac{d^2}{dx^2} \ln cos x = -sec^2 x$,
			the function $f=\ln \cos x$ is concave on all real $x$. This means that
			\[f(A)+f(B)+f(C)\leq 3f(\frac{A+B+C}{3}).\]
			Raising $e$ to the power of both sides of this inequality gives us
			\[\cos A\cos B\cos C\leq \cos(\frac{A+B+C}{3})^3 = \cos(\frac{\pi}{3})^3 = \frac{1}{8},\]
			as desired.
		\end{solu}
	\end{solus}
\end{prob}

% \subsection{Tangent Line Trick}

\section{Solutions}

\printsolu

\end{document}
